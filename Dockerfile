from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="basiliskII"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"


RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
git vde2 libvde-dev libvdeplug-dev autoconf build-essential 

run git clone https://github.com/krechert/macemu.git /macemu

workdir /macemu/BasiliskII/src/Unix
run NO_CONFIGURE=true ./autogen.sh
run ./configure --prefix=/usr --enable-sdl-video --enable-sdl-audio --enable-addressing=real --with-gtk=false --with-mon=false --disable-vosf --with-vdeplug
run make install

add metadata /metadata
